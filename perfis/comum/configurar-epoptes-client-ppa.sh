#!/bin/bash
apt-add-repository ppa:oiteam/epoptes
apt update
apt install epoptes-client
install -m 644 systemd/epoptes-client.service /etc/systemd/system
systemctl stop epoptes-client
systemctl disable epoptes-client
systemctl enable epoptes-client.service
systemctl start epoptes-client.service
